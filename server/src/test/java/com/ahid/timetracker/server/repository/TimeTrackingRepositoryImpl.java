package com.ahid.timetracker.server.repository;

import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.domain.TimeTrackingType;
import com.ahid.timetracker.server.db.exception.NotFoundException;
import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import com.ahid.timetracker.server.db.repo.PassCardRepository;
import com.ahid.timetracker.server.db.repo.TimeTrackingRepository;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class TimeTrackingRepositoryImpl {

    @Inject
    public OrganizationRepository organizationRepository;

    @Inject
    public PassCardRepository passCardRepository;

    @Inject
    public TimeTrackingRepository timeTrackingRepository;


    @Test
    public void allTest() {
        Organization organization = new Organization();
        organization.setName("Org");
        organizationRepository.save(organization);
        organization = organizationRepository.findByName(organization.getName()).get();

        String uid = UUID.randomUUID().toString();
        PassCard passCard = new PassCard();
        passCard.setUid(uid);
        passCard.setFirstName("Фамилия");
        passCard.setLastName("Имя");
        passCard.setMiddleName("Отчество");
        passCard.setType("Преподаватель");
        passCard.setOrganization(organization);
        passCard.setActive(true);
        passCardRepository.save(passCard);

        assertEquals(passCardRepository.findByOrganization(organization.getId()).size(), 1);
        assertNotNull(passCardRepository.findByUidAndActive(uid));

        assertThrows(NotFoundException.class, () -> {
            timeTrackingRepository.create(UUID.randomUUID().toString(), TimeTrackingType.IN_, System.currentTimeMillis());
        });

        for (int i = 0; i < 100; i++) {
            timeTrackingRepository.create(uid, (i & 1) == 1 ? TimeTrackingType.IN_ : TimeTrackingType.OUT, System.currentTimeMillis() - 10000 + i * 10);
        }
        assertTrue(timeTrackingRepository.findAll(organization.getId()).size() == 100);
    }
}

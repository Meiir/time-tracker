package com.ahid.timetracker.server.controller;


import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.domain.Organization;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class AccountControllerTest extends BaseControllerTest {

    @Test
    public void testAll() {

        assertThrows(HttpClientResponseException.class, () -> {
            String invalidCred = buildCred("invalidLogin", "invalidPass");
            assertEquals(accountClient.list(invalidCred, 1).size(), 0);
        });

        String validAdminCred = buildCred("admin", "nimda");
        assertEquals(organizationClient.list(validAdminCred).size(), 0);

        Organization organization = new Organization();
        organization.setName("Org");
        HttpResponse<Organization> resp = organizationClient.save(validAdminCred, organization);
        assertEquals(HttpStatus.CREATED, resp.getStatus());
        assertEquals(organizationClient.list(validAdminCred).size(), 1);

        Long createdOrganizationId = entityId(resp);

        Organization createdOrg = organizationClient.show(validAdminCred, createdOrganizationId);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 0);

        Account account = new Account();
        account.setLogin("login");
        account.setPassword("password");
        account.setActive(true);
        account.setOrganization(createdOrg);
        HttpResponse<Account> saveResponse = accountClient.save(validAdminCred, account);
        long accountId = entityId(saveResponse);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 1);

        Account account2 = new Account();
        account2.setLogin("login2");
        account2.setPassword("password2");
        account2.setActive(true);
        account2.setOrganization(createdOrg);
        HttpResponse<Account> saveResponse2 = accountClient.save(validAdminCred, account2);
        long account2Id = entityId(saveResponse2);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 2);

        account = accountClient.show(validAdminCred, accountId);
        assertEquals(account.getPassword(), "password");
        accountClient.setPassword(validAdminCred, accountId, "new-password");
        account = accountClient.show(validAdminCred, accountId);
        assertEquals(account.getPassword(), "new-password");

        account2 = accountClient.show(validAdminCred, account2Id);
        assertTrue(account2.isActive());
        accountClient.disable(validAdminCred, account2Id);
        account2 = accountClient.show(validAdminCred, account2Id);
        assertFalse(account2.isActive());
        accountClient.enable(validAdminCred, account2Id);
        account2 = accountClient.show(validAdminCred, account2Id);
        assertTrue(account2.isActive());

        accountClient.delete(validAdminCred, accountId);
        accountClient.delete(validAdminCred, account2Id);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 0);

    }
}

package com.ahid.timetracker.server.controller.client;


import com.ahid.timetracker.server.db.domain.Account;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.http.client.annotation.Client;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Client("/account")
public interface AccountClient {

    @Get("/organization/{organizationId}")
    List<Account> list(@Header String authorization, long organizationId);

    @Get("/{id}")
    Account show(@Header String authorization, Long id);

    @Post
    HttpResponse<Account> save(@Header String authorization, @Body @Valid Account account);

    @Post("/{id}/reset-pass")
    HttpResponse setPassword(@Header String authorization, Long id, @NotEmpty String newPassword);

    @Post("/{id}/enable")
    HttpResponse enable(@Header String authorization, Long id);

    @Post("/{id}/disable")
    HttpResponse disable(@Header String authorization, Long id);

    @Delete("/{id}")
    HttpResponse delete(@Header String authorization, Long id);
}

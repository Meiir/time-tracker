package com.ahid.timetracker.server.controller;

import com.ahid.timetracker.server.controller.client.AccountClient;
import com.ahid.timetracker.server.controller.client.OrganizationClient;
import com.ahid.timetracker.server.controller.client.PassCardClient;
import com.ahid.timetracker.server.controller.client.TimeTrackingClient;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;

import javax.inject.Inject;
import java.util.Base64;

abstract public class BaseControllerTest {

    @Inject
    protected OrganizationClient organizationClient;

    @Inject
    protected AccountClient accountClient;

    @Inject
    protected PassCardClient passCardClient;

    @Inject
    protected TimeTrackingClient timeTrackingClient;

    protected String buildCred(String username, String password) {
        byte[] base64Encoded = Base64.getEncoder().encode((username + ":" + password).getBytes());
        String credsEncoded = "Basic " + new String(base64Encoded);
        return credsEncoded;
    }

    protected Long entityId(HttpResponse response) {
        String value = response.header(HttpHeaders.LOCATION);
        if (value == null) {
            return null;
        }
        int index = value.lastIndexOf("/");
        if (index != -1) {
            return Long.valueOf(value.substring(index + 1));
        }
        return null;
    }
}

package com.ahid.timetracker.server.controller.client;

import com.ahid.timetracker.server.db.domain.PassCard;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

import javax.validation.Valid;
import java.util.List;

@Client("/pass-card")
public interface PassCardClient {

    @Get
    List<PassCard> list(@Header String authorization);

    @Get("/{id}")
    PassCard show(@Header String authorization, Long id);

    @Get("/uid/{uid}")
    PassCard showByUid(@Header String authorization, String uid);

    @Post("register")
    HttpResponse<PassCard> register(@Header String authorization, @Body @Valid PassCard passCard);

    @Post("/unregister/{uid}")
    HttpResponse<PassCard> unregister(@Header String authorization, String uid, long deactivatedDate);

    @Get("/type")
    List<String> types(@Header String authorization);
}

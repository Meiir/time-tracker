package com.ahid.timetracker.server.controller;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class PassCardControllerTest extends BaseControllerTest {

    @Test
    public void testAll() {

        String validAdminCred = buildCred("admin", "nimda");

        // by admin should be error of access
        assertThrows(HttpClientResponseException.class, () -> {
            assertEquals(passCardClient.list(validAdminCred).size(), 0);
        });


        Organization organization = new Organization();
        organization.setName("Org");
        HttpResponse<Organization> resp = organizationClient.save(validAdminCred, organization);
        assertEquals(HttpStatus.CREATED, resp.getStatus());
        assertEquals(organizationClient.list(validAdminCred).size(), 1);
        Long createdOrganizationId = entityId(resp);
        Organization createdOrg = organizationClient.show(validAdminCred, createdOrganizationId);

        Account account = new Account();
        account.setLogin("account");
        account.setPassword("account");
        account.setActive(true);
        account.setOrganization(createdOrg);
        HttpResponse<Account> saveResponse = accountClient.save(validAdminCred, account);
        long accountId = entityId(saveResponse);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 1);
        assertEquals(accountClient.show(validAdminCred, accountId).getLogin(), "account");

        String validUserCred = buildCred("account", "account");
        assertEquals(passCardClient.list(validUserCred).size(), 0);

        String uid = UUID.randomUUID().toString();
        PassCard passCard = new PassCard();
        passCard.setUid(uid);
        passCard.setFirstName("Фамилия");
        passCard.setLastName("Имя");
        passCard.setMiddleName("Отчество");
        passCard.setType("Преподаватель");
        passCard.setActive(true);
        HttpResponse<PassCard> saveResponsePassCard = passCardClient.register(validUserCred, passCard);
        long passCardId = entityId(saveResponsePassCard);
        assertEquals(passCardClient.list(validUserCred).size(), 1);
        assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 1);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getAssignedDate(), 0);
        assertEquals(passCardClient.show(validUserCred, passCardId).getDeactivatedDate(), 0);

        // assign passcard with same uid second time
        PassCard passCardSecond = new PassCard();
        passCardSecond.setUid(uid);
        passCardSecond.setFirstName("Фамилия2");
        passCardSecond.setLastName("Имя2");
        passCardSecond.setMiddleName("Отчество2");
        passCardSecond.setType("Студент");
        passCardSecond.setActive(true);
        try {
            HttpResponse<PassCard> saveResponsepassCardSecond = passCardClient.register(validUserCred, passCardSecond);
            assertEquals(1, 2); // this should never run
        } catch (HttpClientResponseException exp) {
            assertEquals(exp.getStatus(), HttpStatus.CONFLICT);
            assertEquals(passCardClient.list(validUserCred).size(), 1);
            assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 1);
        }

        // deactivate
        passCardClient.unregister(validUserCred, uid, System.currentTimeMillis());
        assertEquals(passCardClient.list(validUserCred).size(), 1);
        assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 0);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getAssignedDate(), 0);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getDeactivatedDate(), 0);

        // after deactivating we can assign passcard to another person
        HttpResponse<PassCard> saveResponsepassCardSecond = passCardClient.register(validUserCred, passCardSecond);
        assertEquals(passCardClient.list(validUserCred).size(), 2);
        assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 1);

        assertEquals(passCardClient.types(validUserCred).size(), 2);
    }
}

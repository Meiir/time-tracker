package com.ahid.timetracker.server.controller.client;


import com.ahid.timetracker.server.db.domain.Organization;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.http.client.annotation.Client;

import javax.validation.Valid;
import java.util.List;

@Client("/organization")
public interface OrganizationClient {

    @Get("/")
    List<Organization> list(@Header String authorization);

    @Get("/{id}")
    Organization show(@Header String authorization, Long id);

    @Put("/")
    HttpResponse update(@Header String authorization, @Body @Valid Organization organization);

    @Post("/")
    HttpResponse<Organization> save(@Header String authorization, @Body @Valid Organization organization);

    @Delete("/{id}")
    HttpResponse delete(@Header String authorization, Long id);
}

package com.ahid.timetracker.server.repository;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.repo.AccountRepository;
import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class AccountRepositoryImplTest {

    @Inject
    public AccountRepository accountRepository;

    @Inject
    public OrganizationRepository organizationRepository;

    @Test
    public void allActionsTest() {
        Organization organization = new Organization();
        organization.setName("Org");
        organizationRepository.save(organization);
        organization = organizationRepository.findByName(organization.getName()).get();

        Account account = new Account();
        account.setLogin("login");
        account.setPassword("password");
        account.setOrganization(organization);
        account.setActive(true);
        accountRepository.save(account);

        assertEquals(accountRepository.findAll().size(), 1);
        assertEquals(accountRepository.findAll().get(0).getLogin(), "login");
        assertEquals(accountRepository.findAll().get(0).getOrganization().getName(), "Org");
        assertTrue(accountRepository.findAll().get(0).isActive());

        List<Account> byOrganization = accountRepository.findByOrganization(organization.getId());
        assertEquals(byOrganization.size(), 1);
        assertEquals(accountRepository.findAll().get(0).getLogin(), "login");
        assertEquals(accountRepository.findAll().get(0).getOrganization().getName(), "Org");
        assertTrue(accountRepository.findAll().get(0).isActive());

        long id = accountRepository.findAll().get(0).getId();
        assertTrue(accountRepository.findById(id) != null);

        String login = accountRepository.findAll().get(0).getLogin();
        assertTrue(accountRepository.findByLogin(login) != null);

        accountRepository.updatePassword(id, "other-password");
        assertEquals(accountRepository.findById(id).getPassword(), "other-password");

        accountRepository.updateActive(id, false);
        assertFalse(accountRepository.findById(id).isActive());

        accountRepository.updateActive(id, true);
        assertTrue(accountRepository.findById(id).isActive());

        accountRepository.deleteById(id);
        assertFalse(accountRepository.findById(id) != null);
    }
}
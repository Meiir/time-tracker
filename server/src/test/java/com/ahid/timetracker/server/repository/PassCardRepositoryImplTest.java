package com.ahid.timetracker.server.repository;

import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.exception.AlreadyExistsException;
import com.ahid.timetracker.server.db.exception.NotFoundException;
import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import com.ahid.timetracker.server.db.repo.PassCardRepository;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class PassCardRepositoryImplTest {

    @Inject
    public OrganizationRepository organizationRepository;

    @Inject
    public PassCardRepository passCardRepository;


    @Test
    public void allTest() {
        Organization organization = new Organization();
        organization.setName("Org");
        organizationRepository.save(organization);
        organization = organizationRepository.findByName(organization.getName()).get();

        String uid = UUID.randomUUID().toString();
        PassCard passCard = new PassCard();
        passCard.setUid(uid);
        passCard.setFirstName("Фамилия");
        passCard.setLastName("Имя");
        passCard.setMiddleName("Отчество");
        passCard.setType("Преподаватель");
        passCard.setOrganization(organization);
        passCard.setActive(true);
        passCardRepository.save(passCard);

        assertEquals(passCardRepository.findByOrganization(organization.getId()).size(), 1);
        assertNotNull(passCardRepository.findByUidAndActive(uid));
        assertThrows(NotFoundException.class, () -> {
            passCardRepository.findByUidAndActive(UUID.randomUUID().toString());
        });

        Organization finalOrganization = organization;
        assertThrows(AlreadyExistsException.class, () -> {
            PassCard passCardDuplicated = new PassCard();
            passCardDuplicated.setUid(uid);
            passCardDuplicated.setFirstName("Фамилия");
            passCardDuplicated.setLastName("Имя");
            passCardDuplicated.setMiddleName("Отчество");
            passCardDuplicated.setType("Преподаватель");
            passCardDuplicated.setOrganization(finalOrganization);
            passCardDuplicated.setActive(true);
            passCardRepository.save(passCardDuplicated);
        });

        PassCard pc = passCardRepository.findByUidAndActive(uid);
        assertNotNull(pc);
        assertEquals(pc.getFirstName(), "Фамилия");
        assertEquals(pc.getLastName(), "Имя");
        assertEquals(pc.getMiddleName(), "Отчество");
        assertEquals(pc.getType(), "Преподаватель");
        assertEquals(pc.isActive(), true);
        assertEquals(pc.getOrganization(), organization);
        assertTrue(pc.getAssignedDate() < System.currentTimeMillis());
        assertTrue(pc.getDeactivatedDate() == 0);

        passCardRepository.deactivate(pc.getUid(), System.currentTimeMillis() - 1);
        assertThrows(NotFoundException.class, () -> {
            passCardRepository.findByUidAndActive(UUID.randomUUID().toString());
        });
        List<PassCard> passCards = passCardRepository.findByOrganization(organization.getId());
        assertEquals(passCards.size(), 1);
        PassCard deactivated = passCards.get(0);
        assertNotNull(deactivated);
        assertEquals(deactivated.getFirstName(), "Фамилия");
        assertEquals(deactivated.getLastName(), "Имя");
        assertEquals(deactivated.getMiddleName(), "Отчество");
        assertEquals(deactivated.getType(), "Преподаватель");
        assertEquals(deactivated.isActive(), false);
        assertEquals(deactivated.getOrganization(), organization);
        assertTrue(deactivated.getAssignedDate() < System.currentTimeMillis());
        assertTrue(deactivated.getDeactivatedDate() != 0);

        PassCard passCard2 = new PassCard();
        passCard2.setUid(UUID.randomUUID().toString());
        passCard2.setFirstName("Фамилия");
        passCard2.setLastName("Имя");
        passCard2.setMiddleName("Отчество");
        passCard2.setType("Преподаватель");
        passCard2.setOrganization(organization);
        passCard2.setActive(true);
        passCardRepository.save(passCard);
        assertEquals(passCardRepository.findAllTypes(organization.getId()).size(), 1);
    }
}

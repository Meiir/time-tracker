package com.ahid.timetracker.server.controller.client;

import com.ahid.timetracker.server.db.domain.TimeTracking;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

import javax.annotation.Nullable;
import java.util.List;

@Client("/time-tracking")
public interface TimeTrackingClient {

    @Get
    List<TimeTracking> list(@Header String authorization);

    @Post("/{uid}/in{/time}")
    HttpResponse<?> in(@Header String authorization, String uid, @Nullable Long time);

    @Post("/{uid}/out{/time}")
    HttpResponse<?> out(@Header String authorization, String uid, @Nullable Long time);
}

package com.ahid.timetracker.server.repository;

import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@MicronautTest
public class OrganizationRepositoryImplTest {

    @Inject
    public OrganizationRepository organizationRepository;

    @Test
    public void constraintsAreValidatedForFindById() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.findById(null);
        });
    }

    @Test
    public void constraintsAreValidatedForDeleteById() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.deleteById(null);
        });
    }

    @Test
    public void constraintsAreValidatedForSave() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.save(null);
        });
    }

    @Test
    public void constraintsAreValidatedForUpdateNameIsNull() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.update(12L, null);
        });
    }

    @Test
    public void constraintsAreValidatedForUpdateIdIsNull() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.update(null, null);
        });
    }


    @Test
    public void constraintsAreValidatedForUpdateNameIsBlank() {
        assertThrows(ConstraintViolationException.class, () -> {
            organizationRepository.update(4L, null);
        });
    }
}

package com.ahid.timetracker.server.controller;

import com.ahid.timetracker.server.db.domain.Organization;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@MicronautTest
public class OrganizationControllerTest extends BaseControllerTest {

    @Test
    public void testAll() {

        assertThrows(HttpClientResponseException.class, () -> {
            String invalidCred = buildCred("invalidLogin", "invalidPass");
            assertEquals(organizationClient.list(invalidCred).size(), 0);
        });

        String validAdminCred = buildCred("admin", "nimda");
        assertEquals(organizationClient.list(validAdminCred).size(), 0);

        Organization organization = new Organization();
        organization.setName("Test");
        HttpResponse<Organization> resp = organizationClient.save(validAdminCred, organization);
        assertEquals(HttpStatus.CREATED, resp.getStatus());

        assertEquals(organizationClient.list(validAdminCred).size(), 1);
        Organization created = organizationClient.list(validAdminCred).get(0);
        assertEquals(created.getName(), "Test");
        created.setName("Other");

        HttpResponse updateResp = organizationClient.update(validAdminCred, created);
        assertEquals(HttpStatus.NO_CONTENT, updateResp.getStatus());
        assertEquals(organizationClient.list(validAdminCred).size(), 1);

        HttpResponse deleteResp = organizationClient.delete(validAdminCred, created.getId());
        assertEquals(HttpStatus.NO_CONTENT, deleteResp.getStatus());
        assertEquals(organizationClient.list(validAdminCred).size(), 0);
    }

}

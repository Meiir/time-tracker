package com.ahid.timetracker.server.controller;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class TimeTrackingControllerTest extends BaseControllerTest {


    @Test
    public void testAll() {

        String validAdminCred = buildCred("admin", "nimda");

        // by admin should be error of access
        assertThrows(HttpClientResponseException.class, () -> {
            assertEquals(timeTrackingClient.list(validAdminCred).size(), 0);
        });

        Organization organization = new Organization();
        organization.setName("Org");
        HttpResponse<Organization> resp = organizationClient.save(validAdminCred, organization);
        assertEquals(HttpStatus.CREATED, resp.getStatus());
        assertEquals(organizationClient.list(validAdminCred).size(), 1);
        Long createdOrganizationId = entityId(resp);
        Organization createdOrg = organizationClient.show(validAdminCred, createdOrganizationId);

        Account account = new Account();
        account.setLogin("account");
        account.setPassword("account");
        account.setActive(true);
        account.setOrganization(createdOrg);
        HttpResponse<Account> saveResponse = accountClient.save(validAdminCred, account);
        long accountId = entityId(saveResponse);
        assertEquals(accountClient.list(validAdminCred, createdOrg.getId()).size(), 1);
        assertEquals(accountClient.show(validAdminCred, accountId).getLogin(), "account");

        String validUserCred = buildCred("account", "account");
        assertEquals(passCardClient.list(validUserCred).size(), 0);

        String uid = UUID.randomUUID().toString();
        PassCard passCard = new PassCard();
        passCard.setUid(uid);
        passCard.setFirstName("Фамилия");
        passCard.setLastName("Имя");
        passCard.setMiddleName("Отчество");
        passCard.setType("Преподаватель");
        passCard.setActive(true);
        HttpResponse<PassCard> saveResponsePassCard = passCardClient.register(validUserCred, passCard);
        long passCardId = entityId(saveResponsePassCard);
        assertEquals(passCardClient.list(validUserCred).size(), 1);
        assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 1);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getAssignedDate(), 0);
        assertEquals(passCardClient.show(validUserCred, passCardId).getDeactivatedDate(), 0);

        for (int i = 0; i < 100; i++) {
            timeTrackingClient.in(validUserCred, uid, System.currentTimeMillis() - i);
            timeTrackingClient.out(validUserCred, uid, System.currentTimeMillis() - i);
        }
        assertTrue(timeTrackingClient.list(validUserCred).size() == 200);

        // deactivate
        passCardClient.unregister(validUserCred, uid, System.currentTimeMillis());
        assertEquals(passCardClient.list(validUserCred).size(), 1);
        assertEquals(passCardClient.list(validUserCred).stream().filter(pc -> pc.isActive()).count(), 0);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getAssignedDate(), 0);
        assertNotEquals(passCardClient.show(validUserCred, passCardId).getDeactivatedDate(), 0);

        // after deactivating tracking creating should be return error
        try {
            HttpResponse timeTrackerRsponse = timeTrackingClient.in(validUserCred, uid, System.currentTimeMillis());
            assertEquals(1, 2); // this should never run
        } catch (HttpClientResponseException exp) {
            assertEquals(exp.getStatus(), HttpStatus.NOT_FOUND);
        }
    }
}

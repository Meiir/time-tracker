package com.ahid.timetracker.server;

import com.ahid.timetracker.server.db.MybatisFactory;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.inject.Named;
import javax.sql.DataSource;

@Factory
@Replaces(factory = MybatisFactory.class)
public class MyBatisFactoryForTest {

    private final DataSource dataSource;

    public MyBatisFactoryForTest(@Named("test") DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    SqlSessionFactory sqlSessionFactory() {
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("test", transactionFactory, dataSource);
        Configuration configuration = new Configuration(environment);
        configuration.addMappers("com.ahid.timetracker.server");

        return new SqlSessionFactoryBuilder().build(configuration);
    }
}

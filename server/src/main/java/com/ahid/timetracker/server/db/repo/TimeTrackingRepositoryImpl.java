package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.domain.TimeTracking;
import com.ahid.timetracker.server.db.domain.TimeTrackingType;
import com.ahid.timetracker.server.db.mapper.TimeTrackingMapper;
import io.micronaut.validation.Validated;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@Validated
public class TimeTrackingRepositoryImpl implements TimeTrackingRepository {

    private TimeTrackingMapper timeTrackingMapper;
    private PassCardRepository passCardRepository;

    public TimeTrackingRepositoryImpl(TimeTrackingMapper timeTrackingMapper, PassCardRepository passCardRepository) {
        this.timeTrackingMapper = timeTrackingMapper;
        this.passCardRepository = passCardRepository;
    }

    @Override
    public List<TimeTracking> findAll(long organizationId) {
        return timeTrackingMapper.findAll(organizationId);
    }

    @Override
    public void create(String uid, TimeTrackingType type, long when) {
        PassCard passCard = passCardRepository.findByUidAndActive(uid);
        TimeTracking timeTracking = new TimeTracking();
        timeTracking.setPassCard(passCard);
        timeTracking.setType(type);
        timeTracking.setWhen(when);
        timeTrackingMapper.create(timeTracking);
    }
}

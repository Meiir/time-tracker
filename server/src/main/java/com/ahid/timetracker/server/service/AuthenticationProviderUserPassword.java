package com.ahid.timetracker.server.service;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.repo.AccountRepository;
import io.micronaut.context.annotation.Value;
import io.micronaut.security.authentication.*;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.ahid.timetracker.server.model.UserRoles.ROLE_ADMIN;
import static com.ahid.timetracker.server.model.UserRoles.ROLE_USER;
import static io.micronaut.security.authentication.AuthenticationFailureReason.*;

@Singleton
public class AuthenticationProviderUserPassword implements AuthenticationProvider {

    @Value("${system.admin.login}")
    private String adminLogin;

    @Value("${system.admin.password}")
    private String adminPassword;

    @Inject
    public AccountRepository accountRepository;

    @Override
    public Publisher<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {

        if (authenticationRequest.getIdentity().equals(adminLogin) && authenticationRequest.getSecret().equals(adminPassword)) {
            return Flowable.just(new UserDetails((String) authenticationRequest.getIdentity(), Arrays.asList(ROLE_ADMIN)));
        } else {
            Account account = accountRepository.findByLogin(authenticationRequest.getIdentity().toString());
            if (account != null) {
                if (account.isActive()) {
                    if (account.getPassword().equals(authenticationRequest.getSecret())) {
                        Map<String, Object> attrs = new HashMap<>();
                        attrs.put("organizationId", account.getOrganization().getId());
                        attrs.put("organizationName", account.getOrganization().getName());
                        return Flowable.just(new UserDetails((String) authenticationRequest.getIdentity(), Arrays.asList(ROLE_USER), attrs));
                    } else {
                        return Flowable.just(new AuthenticationFailed(CREDENTIALS_DO_NOT_MATCH));
                    }
                } else {
                    return Flowable.just(new AuthenticationFailed(USER_DISABLED));
                }
            } else {
                return Flowable.just(new AuthenticationFailed(USER_NOT_FOUND));
            }
        }
    }
}

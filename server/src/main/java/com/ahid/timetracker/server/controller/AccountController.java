package com.ahid.timetracker.server.controller;


import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.repo.AccountRepository;
import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.util.List;

import static com.ahid.timetracker.server.model.UserRoles.ROLE_ADMIN;

@Controller("/account")
@Secured({ROLE_ADMIN})
@Validated
public class AccountController {

    protected final AccountRepository accountRepository;
    protected final OrganizationRepository organizationRepository;

    public AccountController(AccountRepository accountRepository, OrganizationRepository organizationRepository) {
        this.accountRepository = accountRepository;
        this.organizationRepository = organizationRepository;
    }

    @Get(value = "/organization/{organizationId}")
    public List<Account> list(long organizationId) {
        return accountRepository.findByOrganization(organizationId);
    }

    @Get("/{id}")
    public Account show(Long id) {
        return accountRepository.findById(id);
    }

    @Post("/")
    public HttpResponse<Account> save(@Body @Valid Account account) {
        accountRepository.save(account);
        Account created = accountRepository.findByLogin(account.getLogin());

        return HttpResponse
                .created(created)
                .headers(headers -> headers.location(location(created.getId())));
    }

    @Post("/{id}/reset-pass")
    public HttpResponse update(Long id, @NotEmpty String newPassword) {
        Account account = accountRepository.findById(id);
        if (account != null) {
            accountRepository.updatePassword(id, newPassword);
            return HttpResponse
                    .noContent()
                    .header(HttpHeaders.LOCATION, location(id).getPath());
        } else {
            return HttpResponse.notFound();
        }
    }

    @Post("/{id}/enable")
    public HttpResponse enable(Long id) {
        return enableOrDisable(id, true);
    }

    @Post("/{id}/disable")
    public HttpResponse disable(Long id) {
        return enableOrDisable(id, false);
    }

    @Delete("/{id}")
    public HttpResponse delete(Long id) {
        accountRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    private HttpResponse enableOrDisable(Long id, boolean isActive) {
        Account account = accountRepository.findById(id);
        if (account != null) {
            accountRepository.updateActive(id, isActive);
            return HttpResponse.noContent();
        } else {
            return HttpResponse.notFound();
        }
    }

    protected URI location(Long id) {
        return URI.create("/account/" + id);
    }
}

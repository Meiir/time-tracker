package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.Organization;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class OrganizationMapperImpl implements OrganizationMapper {

    private final SqlSessionFactory sqlSessionFactory;

    public OrganizationMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    private OrganizationMapper getOrganizationMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(OrganizationMapper.class);
    }

    @Override
    public List<Organization> findAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findAll();
        }
    }

    @Override
    public Organization findById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findById(id);
        }
    }

    @Override
    public Organization findByName(String name) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findByName(name);
        }
    }

    @Override
    public void create(Organization organization) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).create(organization);
            sqlSession.commit();
        }
    }

    @Override
    public void deleteById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).deleteById(id);
            sqlSession.commit();
        }
    }

    @Override
    public void updateName(long id, String name) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).updateName(id, name);
            sqlSession.commit();
        }
    }
}

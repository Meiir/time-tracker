package com.ahid.timetracker.server.db.exception;

import com.ahid.timetracker.server.db.domain.PassCard;

public class AlreadyExistsException extends RuntimeException {

    private PassCard passCard;

    public AlreadyExistsException() {
    }

    public AlreadyExistsException(String message, PassCard passCard) {
        super(message);
        this.passCard = passCard;
    }

    public AlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public PassCard getPassCard() {
        return passCard;
    }
}

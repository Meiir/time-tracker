package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.TimeTracking;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class TimeTrackingMapperImpl implements TimeTrackingMapper {

    private final SqlSessionFactory sqlSessionFactory;

    public TimeTrackingMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    private TimeTrackingMapper getTimeTrackingMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(TimeTrackingMapper.class);
    }

    @Override
    public List<TimeTracking> findAll(long organizationId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getTimeTrackingMapper(sqlSession).findAll(organizationId);
        }
    }

    @Override
    public void create(TimeTracking timeTracking) {

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getTimeTrackingMapper(sqlSession).create(timeTracking);
            sqlSession.commit();
        }
    }
}

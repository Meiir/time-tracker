package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.Organization;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface OrganizationMapper {

    @Select("select * from organizations where id=#{id}")
    Organization findById(long id);

    @Select("select * from organizations where name=#{name}")
    Organization findByName(String name);

    @Insert("insert into organizations(name, description) values(#{name}, #{description})")
    @Options(useGeneratedKeys = true)
    void create(Organization organization);

    @Delete("delete from organizations where id=#{id}")
    void deleteById(long id);

    @Update("update organizations set name=#{name} where id=#{id}")
    void updateName(@Param("id") long id, @Param("name") String name);

    @Select("select * from organizations")
    List<Organization> findAll();
}

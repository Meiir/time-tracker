package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.model.ListingArguments;
import com.ahid.timetracker.server.db.domain.Genre;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface GenreRepository {

    Optional<Genre> findById(@NotNull Long id);

    Genre save(@NotBlank String name);

    void deleteById(@NotNull Long id);

    List<Genre> findAll(@NotNull ListingArguments args);

    int update(@NotNull Long id, @NotBlank String name);
}

package com.ahid.timetracker.server.db.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class Account {

    private long id;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    private String description;

    private boolean active;

    @NotNull
    private Organization organization;

    public Account() {
    }

    public Account(long id, String login, String password, String description, boolean active, Organization organization) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.description = description;
        this.active = active;
        this.organization = organization;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                active == account.active &&
                Objects.equals(login, account.login) &&
                Objects.equals(password, account.password) &&
                Objects.equals(description, account.description) &&
                Objects.equals(organization, account.organization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, description, active, organization);
    }
}

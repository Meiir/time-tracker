package com.ahid.timetracker.server.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.validation.Validated;

import javax.annotation.Nullable;

@Controller("/login")
@Secured("isAuthenticated()")
@Validated
public class LoginController { // This controller simulated auth only. because this use Basic auth.

    @Post
    public HttpResponse login(@Nullable Authentication authentication) {
        return HttpResponse
                .ok(authentication);
    }
}

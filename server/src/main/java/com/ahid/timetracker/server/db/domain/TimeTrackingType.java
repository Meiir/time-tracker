package com.ahid.timetracker.server.db.domain;

public enum TimeTrackingType {
    IN_,
    OUT;

    public String value() {
        return name();
    }

    public static TimeTrackingType fromValue(String v) {
        return valueOf(v);
    }
}

package com.ahid.timetracker.server.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;

import java.security.Principal;

@Secured("isAuthenticated()")
//@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller("/home")
public class HomeController {

    @Get("/user")
        //@Secured(SecurityRule.IS_ANONYMOUS)
    String index(Principal principal) {
        String test = "Test";
        return principal.getName();
    }

    @Get("/admin")
    @Secured({"ROLE_ADMIN", "ROLE_X"})
        //@Secured(SecurityRule.IS_ANONYMOUS)
    String indexAdmin(Principal principal) {
        String test = "Test";
        return principal.getName();
    }
}

package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.PassCard;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface PassCardRepository {

    PassCard findById(@NotNull Long id);

    PassCard findByUidAndActive(@NotEmpty String uid);

    List<PassCard> findAll();

    List<PassCard> findByOrganization(@NotNull Long organizationId);

    List<String> findAllTypes(long organizationId);

    void save(@NotNull PassCard passCard);

    void deactivate(@NotEmpty String uid, @NotNull Long deactivatedDate);
}

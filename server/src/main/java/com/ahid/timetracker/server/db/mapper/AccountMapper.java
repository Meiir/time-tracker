package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.domain.Organization;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface AccountMapper {

    @Select("select * from accounts")
    @Results(id = "account_map", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "description", column = "description"),
            @Result(property = "active", column = "is_active"),
            @Result(property = "organization", javaType = Organization.class,
                    column = "organization_id", one = @One(select = "com.ahid.timetracker.server.db.mapper.OrganizationMapper.findById"))
    })
    List<Account> findAll();

    @Select("select * from accounts where id=#{id}")
    @ResultMap("account_map")
    Account findById(long id);

    @Select("select * from accounts where login=#{login}")
    @ResultMap("account_map")
    Account findByLogin(String login);

    @Select("select * from accounts where organization_id=#{organizationId}")
    @ResultMap("account_map")
    List<Account> findByOrganizationId(long organizationId);

    @Insert("insert into accounts(login, password, description, is_active, organization_id) values(#{login}, #{password}, #{description}, #{active}, #{organization.id})")
    @Options(useGeneratedKeys = true)
    void create(Account account);

    @Delete("delete from accounts where id=#{id}")
    void deleteById(long id);

    @Update("update accounts set password=#{password} where id=#{id}")
    void updatePassword(@Param("id") long id, @Param("password") String password);

    @Update("update accounts set is_active=#{active} where id=#{id}")
    void updateActive(@Param("id") long id, @Param("active") boolean active);
}

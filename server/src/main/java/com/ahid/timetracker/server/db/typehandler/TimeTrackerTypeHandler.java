package com.ahid.timetracker.server.db.typehandler;

import com.ahid.timetracker.server.db.domain.TimeTrackingType;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TimeTrackerTypeHandler implements TypeHandler<TimeTrackingType> {
    @Override
    public void setParameter(PreparedStatement ps, int i, TimeTrackingType parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.name());
    }

    @Override
    public TimeTrackingType getResult(ResultSet rs, String columnName) throws SQLException {
        return TimeTrackingType.fromValue(rs.getString(columnName));
    }

    @Override
    public TimeTrackingType getResult(ResultSet rs, int columnIndex) throws SQLException {
        return TimeTrackingType.fromValue(rs.getString(columnIndex));
    }

    @Override
    public TimeTrackingType getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return TimeTrackingType.fromValue(cs.getString(columnIndex));
    }
}

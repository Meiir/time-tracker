package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PassCardMapper {

    @Select("select * from pass_cards")
    @Results(id = "pass_card_map", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "uid", column = "uid"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "type", column = "type"),
            @Result(property = "active", column = "is_active"),
            @Result(property = "assignedDate", column = "assigned_date"),
            @Result(property = "deactivatedDate", column = "deactivated_date"),
            @Result(property = "organization", javaType = Organization.class,
                    column = "organization_id", one = @One(select = "com.ahid.timetracker.server.db.mapper.OrganizationMapper.findById"))
    })
    List<PassCard> findAll();

    @Select("select * from pass_cards where id=#{id}")
    @ResultMap("pass_card_map")
    PassCard findById(long id);

    @Select("select * from pass_cards where uid=#{uid} and is_active = true")
    @ResultMap("pass_card_map")
    PassCard findByUidAndActive(String uid);

    @Select("select * from pass_cards where organization_id=#{organizationId}")
    @ResultMap("pass_card_map")
    List<PassCard> findByOrganizationId(long organizationId);

    @Select("select distinct type from pass_cards where 0=#{organizationId} or organization_id=#{organizationId}")
    List<String> findAllTypes(long organizationId);

    @Select("select count(*) from pass_cards where uid=#{uid} and is_active = true")
    int countOfActive(String uid);

    @Insert("insert into pass_cards(uid, first_name, last_name, middle_name, type, is_active, assigned_date, organization_id) " +
            "values(#{uid}, #{firstName}, #{lastName}, #{middleName}, #{type}, #{active}, #{assignedDate}, #{organization.id})")
    @Options(useGeneratedKeys = true)
    void create(PassCard passCard);

    @Update("update pass_cards set is_active=false, deactivated_date=#{deactivatedDate} where id=#{id}")
    void deactivate(@Param("id") long id, @Param("deactivatedDate") long deactivatedDate);
}

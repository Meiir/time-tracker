package com.ahid.timetracker.server.model;

public class UserRoles {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
}

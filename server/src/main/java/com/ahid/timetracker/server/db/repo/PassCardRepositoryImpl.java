package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.exception.AlreadyExistsException;
import com.ahid.timetracker.server.db.exception.NotFoundException;
import com.ahid.timetracker.server.db.mapper.PassCardMapper;
import io.micronaut.validation.Validated;

import javax.inject.Singleton;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Singleton
@Validated
public class PassCardRepositoryImpl implements PassCardRepository {

    private PassCardMapper passCardMapper;

    public PassCardRepositoryImpl(PassCardMapper passCardMapper) {
        this.passCardMapper = passCardMapper;
    }

    @Override
    public PassCard findById(@NotNull Long id) {
        return passCardMapper.findById(id);
    }

    @Override
    public PassCard findByUidAndActive(@NotEmpty String uid) {
        PassCard byUidAndActive = passCardMapper.findByUidAndActive(uid);
        if (byUidAndActive == null) {
            throw new NotFoundException("PassCard not found with UID - " + uid);
        }
        return byUidAndActive;
    }

    @Override
    public List<PassCard> findAll() {
        return passCardMapper.findAll();
    }

    @Override
    public List<PassCard> findByOrganization(@NotNull Long organizationId) {
        return passCardMapper.findByOrganizationId(organizationId);
    }

    @Override
    public List<String> findAllTypes(long organizationId) {
        return passCardMapper.findAllTypes(organizationId);
    }

    @Override
    public void save(@NotNull PassCard passCard) {
        if (passCardMapper.countOfActive(passCard.getUid()) == 0) {
            if (!passCard.isActive())
                passCard.setActive(true);
            if (passCard.getAssignedDate() == 0)
                passCard.setAssignedDate(System.currentTimeMillis());
            passCardMapper.create(passCard);
        } else {
            PassCard byUidAndActive = findByUidAndActive(passCard.getUid());
            throw new AlreadyExistsException("Pass card is already existed", byUidAndActive);
        }
    }

    @Override
    public void deactivate(@NotEmpty String uid, @NotNull Long deactivatedDate) {
        PassCard passCard = passCardMapper.findByUidAndActive(uid);
        if (passCard != null) {
            passCardMapper.deactivate(passCard.getId(), deactivatedDate);
        }
    }
}

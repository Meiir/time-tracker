package com.ahid.timetracker.server.db.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class PassCard {

    private long id;
    @NotBlank
    private String uid;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String middleName;
    @NotBlank
    private String type;
    private boolean active;
    @NotNull
    private long assignedDate;
    private long deactivatedDate;
    private Organization organization;

    public PassCard() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(long assignedDate) {
        this.assignedDate = assignedDate;
    }

    public long getDeactivatedDate() {
        return deactivatedDate;
    }

    public void setDeactivatedDate(long deactivatedDate) {
        this.deactivatedDate = deactivatedDate;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassCard passCard = (PassCard) o;
        return id == passCard.id &&
                active == passCard.active &&
                Objects.equals(uid, passCard.uid) &&
                Objects.equals(firstName, passCard.firstName) &&
                Objects.equals(lastName, passCard.lastName) &&
                Objects.equals(middleName, passCard.middleName) &&
                Objects.equals(type, passCard.type) &&
                Objects.equals(assignedDate, passCard.assignedDate) &&
                Objects.equals(deactivatedDate, passCard.deactivatedDate) &&
                Objects.equals(organization, passCard.organization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uid, firstName, lastName, middleName, type, active, assignedDate, deactivatedDate, organization);
    }
}


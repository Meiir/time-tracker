package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.domain.TimeTracking;
import com.ahid.timetracker.server.db.typehandler.TimeTrackerTypeHandler;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface TimeTrackingMapper {

    @Select("select * from time_tracks where pass_card_id in (select id from pass_cards where organization_id=#{organizationId})")
    @Results(id = "time_tracking_map", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "type", column = "type_", typeHandler = TimeTrackerTypeHandler.class),
            @Result(property = "when", column = "when_"),
            @Result(property = "passCard", javaType = PassCard.class,
                    column = "pass_card_id", one = @One(select = "com.ahid.timetracker.server.db.mapper.PassCardMapper.findById"))
    })
    List<TimeTracking> findAll(long organizationId);

    @Insert({"insert into time_tracks(type_, when_, pass_card_id) values(#{type}, #{when},  #{passCard.id})"})
    @ResultMap("time_tracking_map")
    @Options(useGeneratedKeys = true)
    void create(TimeTracking timeTracking);
}

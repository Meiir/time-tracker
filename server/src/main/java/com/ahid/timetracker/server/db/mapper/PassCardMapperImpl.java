package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.PassCard;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class PassCardMapperImpl implements PassCardMapper {

    private final SqlSessionFactory sqlSessionFactory;

    public PassCardMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    private PassCardMapper getPassCardMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PassCardMapper.class);
    }

    @Override
    public List<PassCard> findAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).findAll();
        }
    }

    @Override
    public PassCard findById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).findById(id);
        }
    }

    @Override
    public PassCard findByUidAndActive(String uid) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).findByUidAndActive(uid);
        }
    }

    @Override
    public List<PassCard> findByOrganizationId(long organizationId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).findByOrganizationId(organizationId);
        }
    }

    @Override
    public List<String> findAllTypes(long organizationId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).findAllTypes(organizationId);
        }
    }

    @Override
    public int countOfActive(String uid) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getPassCardMapper(sqlSession).countOfActive(uid);
        }
    }

    @Override
    public void create(PassCard passCard) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getPassCardMapper(sqlSession).create(passCard);
            sqlSession.commit();
        }
    }

    @Override
    public void deactivate(long id, long deactivatedDate) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getPassCardMapper(sqlSession).deactivate(id, deactivatedDate);
            sqlSession.commit();
        }
    }
}

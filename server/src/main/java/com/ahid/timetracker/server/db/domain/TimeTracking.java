package com.ahid.timetracker.server.db.domain;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class TimeTracking {

    private long id;

    @NotNull
    private TimeTrackingType type;

    @NotNull
    private Long when;

    @NotNull
    private PassCard passCard;

    public TimeTracking() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TimeTrackingType getType() {
        return type;
    }

    public void setType(TimeTrackingType type) {
        this.type = type;
    }

    public Long getWhen() {
        return when;
    }

    public void setWhen(Long when) {
        this.when = when;
    }

    public PassCard getPassCard() {
        return passCard;
    }

    public void setPassCard(PassCard passCard) {
        this.passCard = passCard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeTracking that = (TimeTracking) o;
        return id == that.id &&
                type == that.type &&
                Objects.equals(when, that.when) &&
                Objects.equals(passCard, that.passCard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, when, passCard);
    }
}

package com.ahid.timetracker.server.db.mapper;

import com.ahid.timetracker.server.db.domain.Account;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class AccountMapperImpl implements AccountMapper {

    private final SqlSessionFactory sqlSessionFactory;

    public AccountMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    private AccountMapper getOrganizationMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AccountMapper.class);
    }

    @Override
    public List<Account> findAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findAll();
        }
    }

    @Override
    public List<Account> findByOrganizationId(long organizationId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findByOrganizationId(organizationId);
        }
    }

    @Override
    public Account findById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findById(id);
        }
    }

    @Override
    public Account findByLogin(String login) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return getOrganizationMapper(sqlSession).findByLogin(login);
        }
    }

    @Override
    public void create(Account account) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).create(account);
            sqlSession.commit();
        }
    }

    @Override
    public void deleteById(long id) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).deleteById(id);
            sqlSession.commit();
        }
    }

    @Override
    public void updatePassword(long id, String password) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).updatePassword(id, password);
            sqlSession.commit();
        }
    }

    @Override
    public void updateActive(long id, boolean active) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            getOrganizationMapper(sqlSession).updateActive(id, active);
            sqlSession.commit();
        }
    }
}

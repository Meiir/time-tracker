package com.ahid.timetracker.server.controller;

import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.domain.TimeTracking;
import com.ahid.timetracker.server.db.domain.TimeTrackingType;
import com.ahid.timetracker.server.db.exception.NotFoundException;
import com.ahid.timetracker.server.db.repo.TimeTrackingRepository;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.validation.Validated;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.ahid.timetracker.server.model.UserRoles.ROLE_USER;

@Controller("/time-tracking")
@Secured({ROLE_USER})
@Validated
public class TimeTrackingController {

    protected final TimeTrackingRepository timeTrackingRepository;

    public TimeTrackingController(TimeTrackingRepository timeTrackingRepository) {
        this.timeTrackingRepository = timeTrackingRepository;
    }

    @Get
    public List<TimeTracking> list(@NotNull Authentication authentication) {
        if (authentication.getAttributes().get("organizationId") != null) {
            return timeTrackingRepository.findAll(Long.valueOf(String.valueOf(authentication.getAttributes().get("organizationId"))));
        } else {
            return timeTrackingRepository.findAll(0);
        }
    }

    @Post("/{uid}/in{/time}")
    public HttpResponse in(String uid, @Nullable Long time) {
        timeTrackingRepository.create(uid, TimeTrackingType.IN_, time == null ? System.currentTimeMillis() : time);
        return HttpResponse.noContent();
    }

    @Post("/{uid}/out{/time}")
    public HttpResponse out(String uid, @Nullable Long time) {
        timeTrackingRepository.create(uid, TimeTrackingType.OUT, time == null ? System.currentTimeMillis() : time);
        return HttpResponse.noContent();
    }

    @io.micronaut.http.annotation.Error(global = false)
    public HttpResponse<?> exception(HttpRequest request, RuntimeException runtimeException) {
        if (runtimeException instanceof NotFoundException) {
            NotFoundException notFoundException = (NotFoundException) runtimeException;
            return HttpResponse.<PassCard>status(HttpStatus.NOT_FOUND, notFoundException.getMessage());
        } else {
            return HttpResponse.<PassCard>status(HttpStatus.BAD_REQUEST, runtimeException.getMessage());
        }
    }
}

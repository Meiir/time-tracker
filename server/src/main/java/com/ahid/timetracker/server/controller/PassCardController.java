package com.ahid.timetracker.server.controller;


import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.domain.PassCard;
import com.ahid.timetracker.server.db.exception.AlreadyExistsException;
import com.ahid.timetracker.server.db.exception.NotFoundException;
import com.ahid.timetracker.server.db.repo.PassCardRepository;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;

import static com.ahid.timetracker.server.model.UserRoles.ROLE_USER;

@Controller("/pass-card")
@Secured({ROLE_USER})
@Validated
public class PassCardController {

    protected final PassCardRepository passCardRepository;

    public PassCardController(PassCardRepository passCardRepository) {
        this.passCardRepository = passCardRepository;
    }

    @Get
    public List<PassCard> list(@NotNull Authentication authentication) {
        if (authentication.getAttributes().get("organizationId") != null) {
            return passCardRepository.findByOrganization(Long.valueOf(String.valueOf(authentication.getAttributes().get("organizationId"))));
        } else {
            return passCardRepository.findAll();
        }
    }

    @Get("/{id}")
    public PassCard show(Long id) {
        return passCardRepository.findById(id);
    }

    @Get("/uid/{uid}")
    public PassCard showByUid(String uid) {
        return passCardRepository.findByUidAndActive(uid);
    }

    @Post("/register")
    public HttpResponse register(@Body @Valid PassCard passCard, @NotNull Authentication authentication) {
        if (authentication.getAttributes().get("organizationId") != null) {
            Organization organization = new Organization();
            organization.setId(Long.valueOf(String.valueOf(authentication.getAttributes().get("organizationId"))));
            passCard.setOrganization(organization);
        }
        passCardRepository.save(passCard);
        PassCard created = passCardRepository.findByUidAndActive(passCard.getUid());
        return HttpResponse
                .created(created)
                .headers(headers -> headers.location(location(created.getId())));
    }

    @Post("/unregister/{uid}")
    public HttpResponse unregister(String uid, long deactivatedDate) {
        passCardRepository.deactivate(uid, deactivatedDate == 0 ? System.currentTimeMillis() : deactivatedDate);
        return HttpResponse
                .noContent();
    }

    @Get("/type")
    public List<String> getTypes(@NotNull Authentication authentication) {
        return passCardRepository.findAllTypes(authentication.getAttributes().get("organizationId") != null
                ? Long.valueOf(String.valueOf(authentication.getAttributes().get("organizationId")))
                : 0);
    }

    protected URI location(Long id) {
        return URI.create("/pass-card/" + id);
    }

    @io.micronaut.http.annotation.Error(global = false)
    public HttpResponse<?> exception(HttpRequest request, RuntimeException runtimeException) {
        if (runtimeException instanceof AlreadyExistsException) {
            AlreadyExistsException alreadyExistsException = (AlreadyExistsException) runtimeException;
            return HttpResponse.<PassCard>status(HttpStatus.CONFLICT, "PassCard is Already Exists")
                    .body(alreadyExistsException.getPassCard());
        } else if (runtimeException instanceof NotFoundException) {
            NotFoundException notFoundException = (NotFoundException) runtimeException;
            return HttpResponse.<PassCard>status(HttpStatus.NOT_FOUND, notFoundException.getMessage());
        } else {
            return HttpResponse.<PassCard>status(HttpStatus.BAD_REQUEST, runtimeException.getMessage());
        }
    }
}

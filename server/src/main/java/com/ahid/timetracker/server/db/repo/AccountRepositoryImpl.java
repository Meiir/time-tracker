package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.Account;
import com.ahid.timetracker.server.db.mapper.AccountMapper;
import io.micronaut.validation.Validated;

import javax.inject.Singleton;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Singleton
@Validated
public class AccountRepositoryImpl implements AccountRepository {

    private AccountMapper accountMapper;

    public AccountRepositoryImpl(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    @Override
    public Account findById(@NotNull Long id) {
        return accountMapper.findById(id);
    }

    @Override
    public Account findByLogin(@NotNull String login) {
        return accountMapper.findByLogin(login);
    }

    @Override
    public List<Account> findAll() {
        return accountMapper.findAll();
    }

    @Override
    public List<Account> findByOrganization(@NotNull Long organizationId) {
        return accountMapper.findByOrganizationId(organizationId);
    }

    @Override
    public void save(@NotNull Account account) {
        accountMapper.create(account);
    }

    @Override
    public void deleteById(@NotNull Long id) {
        Account account = findById(id);
        if (account != null) {
            accountMapper.deleteById(id);
        }
    }

    @Override
    public void updatePassword(@NotNull Long id, @NotBlank String password) {
        accountMapper.updatePassword(id, password);
    }

    @Override
    public void updateActive(@NotNull Long id, boolean active) {
        accountMapper.updateActive(id, active);
    }
}

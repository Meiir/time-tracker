package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.Organization;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface OrganizationRepository {

    Optional<Organization> findById(@NotNull Long id);

    Optional<Organization> findByName(@NotNull String name);

    void save(@NotNull Organization organization);

    void deleteById(@NotNull Long id);

    List<Organization> findAll();

    void update(@NotNull Long id, @NotNull Organization organization);
}

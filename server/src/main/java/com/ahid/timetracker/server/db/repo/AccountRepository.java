package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.Account;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface AccountRepository {

    Account findById(@NotNull Long id);

    Account findByLogin(@NotNull String login);

    List<Account> findAll();

    List<Account> findByOrganization(@NotNull Long organizationId);

    void save(@NotNull Account account);

    void deleteById(@NotNull Long id);

    void updatePassword(@NotNull Long id, @NotBlank String password);

    void updateActive(@NotNull Long id, boolean active);
}

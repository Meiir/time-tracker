package com.ahid.timetracker.server.controller;


import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.repo.OrganizationRepository;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static com.ahid.timetracker.server.model.UserRoles.ROLE_ADMIN;

@Controller("/organization")
@Secured({ROLE_ADMIN})
@Validated
public class OrganizationController {

    protected final OrganizationRepository organizationRepository;

    public OrganizationController(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Get
    public List<Organization> list() {
        return organizationRepository.findAll();
    }

    @Get("/{id}")
    public Organization show(Long id) {
        return organizationRepository
                .findById(id)
                .orElse(null);
    }

    @Post("/")
    public HttpResponse<Organization> save(@Body @Valid Organization organization) {
        organizationRepository.save(organization);
        Organization created = organizationRepository.findByName(organization.getName()).get();

        return HttpResponse
                .created(created)
                .headers(headers -> headers.location(location(created.getId())));
    }

    @Put("/")
    public HttpResponse update(@Body @Valid Organization organization) {
        organizationRepository.update(organization.getId(), organization);

        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, location(organization.getId()).getPath());
    }

    @Delete("/{id}")
    public HttpResponse delete(Long id) {
        organizationRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    protected URI location(Long id) {
        return URI.create("/organization/" + id);
    }
}
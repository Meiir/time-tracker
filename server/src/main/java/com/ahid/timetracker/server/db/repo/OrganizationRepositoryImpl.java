package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.Organization;
import com.ahid.timetracker.server.db.mapper.OrganizationMapper;
import io.micronaut.validation.Validated;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Singleton
@Validated
public class OrganizationRepositoryImpl implements OrganizationRepository {

    private OrganizationMapper organizationMapper;

    public OrganizationRepositoryImpl(OrganizationMapper organizationMapper) {
        this.organizationMapper = organizationMapper;
    }

    @Override
    public Optional<Organization> findById(@NotNull Long id) {
        return Optional.ofNullable(organizationMapper.findById(id));
    }

    @Override
    public Optional<Organization> findByName(@NotNull String name) {
        return Optional.ofNullable(organizationMapper.findByName(name));
    }

    @Override
    public void save(@NotNull Organization organization) {
        organizationMapper.create(organization);
    }

    @Override
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(genre -> organizationMapper.deleteById(id));
    }

    @Override
    public List<Organization> findAll() {
        return organizationMapper.findAll();
    }

    @Override
    public void update(@NotNull Long id, @NotNull Organization organization) {
        organizationMapper.updateName(id, organization.getName());
    }
}

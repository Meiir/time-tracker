package com.ahid.timetracker.server.db.repo;

import com.ahid.timetracker.server.db.domain.TimeTracking;
import com.ahid.timetracker.server.db.domain.TimeTrackingType;

import java.util.List;

public interface TimeTrackingRepository {

    List<TimeTracking> findAll(long organizationId);

    void create(String uid, TimeTrackingType type, long when);
}
